import React from 'react';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container} from 'react-bootstrap';
import Navigation from './components/Navigation';
import HomePage from './components/HomePage';
import VideoPage from './components/VideoPage';
import AuthPage from './components/AuthPage';
import AccountPage from './components/AccountPage';
import PostOfCard from './components/post/PostOfCard';
import WelcomePageAuthPage from './components/Athentication/WelcomePageAuthPage';
import {ProtectRoute} from './components/Athentication/ProtectRoute';

function App() {
  return (
    <Router>
      <div className="App">
      <Container fluid className="navi">
        <Container>
          <Navigation />
        </Container>
      </Container>

      <Container fluid className="content">
        <Container>
          <Switch>
            <Route path="/" exact component={MainPage}/>
            <Route path='/home' component={HomePage}/>
            <Route path='/video' exact component={VideoPage}/>
            <Route path='/account' exact component={AccountPage}/>
            <Route path='/auth' exact component={AuthPage}/>
            <ProtectRoute path='/welcome' component={WelcomePageAuthPage}/>
            <Route path="/post/:id" component={PostOfCard}/>
            <Route path="*" exact component={Notfound}/>
          </Switch>
          </Container>
        </Container>
      </div>
    </Router>
  );
}

const MainPage = () => (
  <div>
    <h1>Welcome to our Default Page</h1>
  </div>
);

const Notfound = () => (
  <div>
    <h1>Error 404 Page !!!</h1>
  </div>
);

export default App;