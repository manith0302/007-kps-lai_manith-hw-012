import React from 'react';
import '../App.css';
import {NavLink} from 'react-router-dom';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import AnimationCategory from "./video/AnimationCategory";
import MovieCategory from "./video/MovieCategory";

function VideoPage() {

  return (
    <Router>
    <div className="video">
      <ul>
        <li>
          <NavLink to='/video/animation' exact activeStyle={{color: '#E53935'}}>Animation</NavLink>
        </li>
        <li>
          <NavLink to='/video/movie' exact activeStyle={{color: '#E53935'}}>Movie</NavLink>
        </li>
      </ul>
      <Switch>
        <Route path='/video' exact component={SelectOption}/>
        <Route path='/video/animation' component={AnimationCategory}/>
        <Route path='/video/movie' component={MovieCategory}/>
      </Switch>
    </div>
    </Router>
  );
}

const SelectOption = () => (
  <div>
    <h4>Please Select a Option</h4>
  </div>
);

export default VideoPage;