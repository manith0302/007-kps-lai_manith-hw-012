import React from 'react'
import queryString from "query-string";

function QAN(props) {

    let items = queryString.parse(props.location.search);

    return (
        <div>
            <h4>The name of query string is " <span className='upper-text'>{items.name}</span> "</h4>
        </div>
    )
}

export default QAN;
