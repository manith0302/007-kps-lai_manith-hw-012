import React from 'react'

function PostOfCard({match}) {
    return (
        <div>
            <h4>This is post of number {match.params.id}</h4>
        </div>
    )
}

export default PostOfCard
