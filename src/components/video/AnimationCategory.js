import React from 'react';
import '../../App.css';
import {NavLink} from 'react-router-dom';
import {BrowserRouter as Router, Route} from 'react-router-dom';

function AnimationCategory() {
  return (
    <Router>
    <div>
        <h2>Animation Category</h2>
        <ul>
            <li>
                <NavLink to="/video/animation/action" exact activeStyle={{color: '#E53935'}}>Action</NavLink>
            </li>
            <li>
                <NavLink to="/video/animation/romance" exact activeStyle={{color: '#E53935'}}>Romance</NavLink>
            </li>
            <li>
                <NavLink to="/video/animation/comedy" exact activeStyle={{color: '#E53935'}}>Comedy</NavLink>
            </li>
        </ul>
        <Route path='/video/animation' exact component={SelectOption}/>
        <Route path='/video/animation/:name' component={Name}/>
    </div>
    </Router>
  );
}
const Name = ({match}) => (
    <div>
        <h4>The Option is <span className="upper-text">{match.params.name}</span></h4>
    </div>
);

const SelectOption = () => (
    <div>
        <h4>Please Select a Option</h4>
    </div>
);

export default AnimationCategory;