import React from 'react';
import '../../App.css';
import {NavLink} from 'react-router-dom';
import {BrowserRouter as Router, Route} from 'react-router-dom';

function MovieCategory() {
    return (
        <Router>
            <div>
                <h2>Movie Category</h2>
                <ul>
                    <li>
                        <NavLink to='/video/movie/adventure' exact activeStyle={{color: '#E53935'}}>Adventure</NavLink>
                    </li>
                    <li>
                        <NavLink to='/video/movie/comedy' exact activeStyle={{color: '#E53935'}}>Comedy</NavLink>
                    </li>
                    <li>
                        <NavLink to='/video/movie/crime' exact activeStyle={{color: '#E53935'}}>Crime</NavLink>
                    </li>
                    <li>
                        <NavLink to='/video/movie/horror' exact activeStyle={{color: '#E53935'}}>Horror</NavLink>
                    </li>
                </ul>
                <Route path='/video/movie' exact component={SelectOptionMovie} />
                <Route path='/video/movie/:category' component={MovieCategories} />
            </div>
        </Router>
    )
}

const MovieCategories = ({match}) => (
    <div>
        <h4>The Option is <span className="upper-text">{match.params.category}</span></h4>
    </div>
);

const SelectOptionMovie = () => (
    <div>
        <h4>Please Select a Option</h4>
    </div>
);

export default MovieCategory
