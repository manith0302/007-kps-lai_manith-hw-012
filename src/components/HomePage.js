import React from 'react';
import '../App.css';
import {Card, CardDeck, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

function HomePage() {
  return (
    <div>
        <CardDeck>
          <Card>
            <Card.Img variant="top" src="https://www.fcbarcelona.com/photo-resources/2020/05/30/2f3ee7b0-fb40-48cc-8dad-b6ce9de34551/mini_2020-05-30-ENTRENO-08.JPG?width=1200&height=750" />
            <Card.Body>
              <Card.Title>Card title</Card.Title>
              <Card.Text>
                This is a wider card with supporting text below as a natural lead-in to
                additional content. This content is a little bit longer.
                <br/>
                <Button variant="light" as={Link} to='/post/1'>View More</Button>
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">Last updated 3 mins ago</small>
            </Card.Footer>
          </Card>
          <Card>
            <Card.Img variant="top" src="https://www.fcbarcelona.com/photo-resources/2020/05/30/689da316-8dd9-49fe-816f-1b97ec72f1f1/mini_2020-05-30-ENTRENO-14.JPG?width=1200&height=750" />
            <Card.Body>
              <Card.Title>Card title</Card.Title>
              <Card.Text>
                This is a wider card with supporting text below as a natural lead-in to
                additional content. This content is a little bit longer.
                <br/>
                <Button variant="light" as={Link} to='/post/2'>View More</Button>
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">Last updated 3 mins ago</small>
            </Card.Footer>
          </Card>
          <Card>
            <Card.Img variant="top" src="https://www.fcbarcelona.com/photo-resources/2020/05/30/cd59e2cb-1cec-4510-9319-38bdcdce3646/mini_2020-05-30-ENTRENO-134.JPG?width=1200&height=750" />
            <Card.Body>
              <Card.Title>Card title</Card.Title>
              <Card.Text>
                This is a wider card with supporting text below as a natural lead-in to
                additional content. This content is a little bit longer.
                <br/>
                <Button variant="light" as={Link} to='/post/3'>View More</Button>
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">Last updated 3 mins ago</small>
            </Card.Footer>
          </Card>
          <Card>
            <Card.Img variant="top" src="https://www.fcbarcelona.com/photo-resources/2020/05/30/938555ed-e84a-4806-9edb-6190eb64b747/mini_2020-05-30-ENTRENO-139.JPG?width=1200&height=750" />
            <Card.Body>
              <Card.Title>Card title</Card.Title>
              <Card.Text>
                This is a wider card with supporting text below as a natural lead-in to
                additional content. This content is a little bit longer.
                <br/>
                <Button variant="light" as={Link} to='/post/4'>View More</Button>
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">Last updated 3 mins ago</small>
            </Card.Footer>
          </Card>
          <Card>
            <Card.Img variant="top" src="https://www.fcbarcelona.com/photo-resources/2020/05/30/158d084a-d679-40c0-8b81-f1fad880add5/mini_2020-05-30-ENTRENO-28.JPG?width=1200&height=750" />
            <Card.Body>
              <Card.Title>Card title</Card.Title>
              <Card.Text>
                This is a wider card with supporting text below as a natural lead-in to
                additional content. This content is a little bit longer.
                <br/>
                <Button variant="light" as={Link} to='/post/5'>View More</Button>
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">Last updated 3 mins ago</small>
            </Card.Footer>
          </Card>
          <Card>
            <Card.Img variant="top" src="https://www.fcbarcelona.com/photo-resources/2020/05/30/84457a22-7814-466a-b52c-0046c2ecdaa6/mini_2020-05-30-ENTRENO-108.JPG?width=1200&height=750" />
            <Card.Body>
              <Card.Title>Card title</Card.Title>
              <Card.Text>
                This is a wider card with supporting text below as a natural lead-in to
                additional content. This content is a little bit longer.
                <br/>
                <Button variant="light" as={Link} to='/post/6'>View More</Button>
              </Card.Text>
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">Last updated 3 mins ago</small>
            </Card.Footer>
          </Card>
        </CardDeck>
    </div>
  );
}

export default HomePage;
