import React from 'react';
import '../App.css';
import {Form, Button} from 'react-bootstrap';
import Auth from "./Athentication/Auth";

function AuthPage(props) {

  return (
    <Form>
      <Form.Group controlId="formGroupEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" />
      </Form.Group>
      <Form.Group controlId="formGroupPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" />
      </Form.Group>
      <Button variant="light" onClick={() => {

        Auth.login(() => {
          props.history.push('/welcome');
        });

      }}>Submit</Button>
    </Form>
  );
}

export default AuthPage;