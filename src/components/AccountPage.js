import React,{useState} from 'react';
import '../App.css';
import {Link} from "react-router-dom";
import {BrowserRouter as Router, Route} from 'react-router-dom';
import QAN from './QueryAccountName/QAN';

function AccountPage() {

  let [getValue, setValue] = useState(false);

  const ThisClick = () => {
    setValue(true)
  }

  return (
    <Router>
    <div>
      <h2>Accounts</h2>
      <ul onClick={ThisClick}>
        <li>
          <Link to='/account?name=netflix'>Netflix</Link>
        </li>
        <li>
          <Link to='/account?name=instagram'>Instagram</Link>
        </li>
        <li>
          <Link to='/account?name=twitter'>Twitter</Link>
        </li>
        <li>
          <Link to='/account?name=whatapp'>WhatApp</Link>
        </li>
        <li>
          <Link to='/account?name=facebook'>Facebook</Link>
        </li>
      </ul>
      <Route path='/account' exact component={getValue ? QAN : noQueryString}/>
    </div>
    </Router>
  );
}

const noQueryString = () => (
  <h4>There is no name query string.</h4>
);

export default AccountPage;