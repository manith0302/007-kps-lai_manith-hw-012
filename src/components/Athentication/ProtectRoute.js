import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Auth from './Auth';

export const ProtectRoute = ({component : Component, ...rest}) => {
    return(
        <Route
            {...rest}
            render = {props => {
                if (Auth.isAuthenticate()){
                    return <Component {...props} />;
                } else{
                    return(
                        <Redirect 
                            to = {
                                {
                                    pathname : "/auth",
                                    state : {
                                        from : props.location
                                    }
                                }
                            }
                        />
                    );
                }
            }}
        />
    );
}
