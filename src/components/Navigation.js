import React from 'react';
import '../App.css';
import {NavLink} from 'react-router-dom';
import {Navbar, Nav, Button, Form, FormControl} from 'react-bootstrap';

function MyNav() {

  return (
  <Navbar bg="dark" variant="dark">
    <Navbar.Brand as={NavLink} to="/">React Router</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link as={NavLink} to="/home" exact activeStyle={{color: '#E53935'}}>Home</Nav.Link>
      <Nav.Link as={NavLink} to="/video" exact activeStyle={{color: '#E53935'}}>Video</Nav.Link>
      <Nav.Link as={NavLink} to="/account" exact activeStyle={{color: '#E53935'}}>Account</Nav.Link>
      <Nav.Link as={NavLink} to="/auth" exact activeStyle={{color: '#E53935'}}>Auth</Nav.Link> 
    </Nav>
    <Form inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
      <Button variant="outline-info">Search</Button>
    </Form>
  </Navbar>

  );
}

export default MyNav;